﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Globalization;

namespace greeeed
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void GetFileName()
        {
            string filename="";
            OpenFileDialog openFileDialog1 = new OpenFileDialog() { Filter = "Файлы образа|*.001" };
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
                filename = openFileDialog1.FileName;
            textBox1.Text = filename;
        } // получили имя файла

        private void button1_Click(object sender, EventArgs e)
        {
            GetFileName();
            button2.Enabled = true;
        }// записали имя файла

        private void button2_Click(object sender, EventArgs e)
        {
            listBox1.Visible = true;
            listBox1.Items.Clear();
            //================= дальше хуже
            string filename = textBox1.Text;
            FileStream fileStream = new FileStream(filename, FileMode.Open);
            /* открытие не проверяем, ибо файл открыт насильно и явно */


            for (int i = 450; i < 500; i += 16)
            {
                fileStream.Seek(i, SeekOrigin.Begin); // ищем точку монтирования
                byte[] NTFSmark = new byte[1];
                fileStream.Read(NTFSmark, 0, 1);

                if (NTFSmark[0] == 7)
                {
                    listBox1.Items.Add("Partition"); // проверяем NTFS и записываем его наличие
                }
            } // записали все boot-сектора
            fileStream.Close();
        }

        private int GetAdress(byte[] s5)
        {
            string s11 = new string('0', 2);
            string s12 = new string('0', 2);
            string s13 = new string('0', 2);
            string s14 = new string('0', 2);
            s11 = s5[0].ToString();
            s12 = s5[1].ToString();
            s13 = s5[2].ToString();
            s14 = s5[3].ToString();

            int a1, a2, a3, a4;
            a1 = int.Parse(s11);
            a2 = int.Parse(s12) * 256;
            a3 = int.Parse(s13) * 256 * 256;
            a4 = int.Parse(s14) * 256 * 256 * 256;
            int a = (a1 + a2 + a3 + a4) * 512;
            return a;
        }

        private int GetMFT(byte[] s6)
        {
            int a = s6[0] + s6[1] * (int)(Math.Pow(256, 1)) + s6[2] * (int)(Math.Pow(256, 2)) + s6[3] * (int)(Math.Pow(256, 3)) + s6[4] * (int)(Math.Pow(256, 4));
            // да, явно не до конца, но для флешки на 256 метров самое ок
            return a;
        }

        private int AnalyseFILE0(int adress, FileStream FileStream)
        {
            FileStream.Seek(adress + 21, SeekOrigin.Begin);
            byte[] flag = new byte[2];
            FileStream.Read(flag, 0, 2);
            int flagRes = (flag[0]) * 256 + (flag[1]);
            if (flagRes == 0)
                return -1; //пустышка
            if ((flagRes == 1)||(flagRes==9))
            {
                FileStream.Seek(adress + 242, SeekOrigin.Begin);
                byte[] NameFlag = new byte[1];
                FileStream.Read(NameFlag, 0, 1);
                FileStream.Seek(adress + 218, SeekOrigin.Begin);
                byte[] NameFlag2 = new byte[1];
                FileStream.Read(NameFlag2, 0, 1);
                int NameFlagRes = NameFlag[0];
                int NameFlagRes2 = NameFlag2[0];
                if (NameFlagRes == 36)
                    return 4;//служебный
                if (NameFlagRes2 == 36)
                    return 41;
                else
                    return 1;//файл
            }

            if (flagRes == 2)
                return 2;//папка
            if (flagRes == 3)
                return 3;//описатель
            return 0;
        }

        public struct StandardInformation
        {
            public ulong FileCreationTime;
            public ulong FileAlterationTime;
            public ulong MFTChangeTime;
            public ulong FileReadTime;
        } // Standard Infomation;

        public struct FileInformation
        {
            public ulong FileCreationTime;
            public ulong FileModificationTime;
            public ulong FileAccessTime;
            public ulong MFTModificationTime;
        } // File information;

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox2.Items.Clear();
            listBox3.Items.Clear();
            listBox4.Items.Clear();
            listBox5.Items.Clear();
            listBox6.Items.Clear();
            listBox7.Items.Clear();
            listBox8.Items.Clear();
            listBox9.Items.Clear();
            listBox10.Items.Clear();
            listBox11.Visible = true;
            listBox12.Visible = true;
            listBox13.Visible = true;
            listBox14.Visible = true;
            listBox12.Items.Clear();
            int marker = this.listBox1.SelectedIndex;
            if (marker == -1)
                marker = -454;
            int boot = 454 + marker * 16;
            if (boot > 0)
            {
                splitContainer1.Visible = true;
                byte[] adress = new byte[4];
                byte[] size = new byte[4];
                byte[] MFT = new byte[8];
                string filename = textBox1.Text;
                FileStream fileStream = new FileStream(filename, FileMode.Open);
                fileStream.Seek(boot, SeekOrigin.Begin);
                fileStream.Read(adress, 0, 4);
                fileStream.Seek(boot + 4, SeekOrigin.Begin);
                fileStream.Read(size, 0, 4);
                int realadress = GetAdress(adress);
                int partsize = GetAdress(size);
                listBox2.Items.Add("File System:");
                listBox3.Items.Add("NTFS");
                listBox2.Items.Add("Partition size:");
                listBox3.Items.Add(partsize);
                listBox2.Items.Add("Boot Sector[dec]:");
                listBox3.Items.Add(realadress);
                string hexrealadress = Convert.ToString(realadress, 16);
                listBox2.Items.Add("Boot Sector[hex]:");
                listBox3.Items.Add(hexrealadress);
                int adr = realadress + 48; // вышли на 1 кусок MFT
                fileStream.Seek(adr, SeekOrigin.Begin);
                fileStream.Read(MFT, 0, 8);
                int mftaddr = GetMFT(MFT);
                listBox2.Items.Add("MFT[dec]:");
                listBox3.Items.Add(mftaddr);
                string hexmftaddr = Convert.ToString(mftaddr, 16);
                listBox2.Items.Add("MFT[hex]:");
                listBox3.Items.Add(hexmftaddr);
                int File0location = mftaddr * 512 + realadress;
                string hexfile0 = Convert.ToString(File0location, 16);
                listBox2.Items.Add("File0 location:");
                listBox3.Items.Add(hexfile0); //вышли на адрес файлов
                int Res = 1488;
                splitContainer2.Visible = true;
                splitContainer3.Visible = true;
                splitContainer4.Visible = true;
                listBox10.Visible = true;
                //===================================================================================================================
                for (int i = File0location; i < (File0location + 100000000); i += 1024)
                {
                    Res = AnalyseFILE0(i, fileStream);
                    if (Res == -1)
                        continue;                                   // обработка HOLLOW'S

                    if (Res == 4)
                    {
                        fileStream.Seek(i + 242, SeekOrigin.Begin);
                        byte[] Name = new byte[226];
                        fileStream.Read(Name, 0, 226);
                        string listname="";

                        for (int j = 0; j < 226; j += 2)
                        {
                            int b = Name[j];
                            char a = (char)b;
                            listname += a;
                        }

                        listBox4.Items.Add(listname);
                        listBox5.Items.Add(Convert.ToString(i, 16));
                    }                                               // обработка служебных адекватных

                    if (Res == 41)
                    {
                        fileStream.Seek(i + 218, SeekOrigin.Begin);
                        byte[] Name = new byte[226];
                        fileStream.Read(Name, 0, 226);
                        string listname = "";

                        for (int j = 0; j < 226; j += 2)
                        {
                            int b = Name[j];
                            char a = (char)b;
                            listname += a;
                        }

                        listBox4.Items.Add(listname);
                        listBox5.Items.Add(Convert.ToString(i, 16));
                    }                                               //обработка служебных дебилов

                    if (Res == 1)
                    {
                        fileStream.Seek(i + 242, SeekOrigin.Begin);
                        byte[] Name = new byte[226];
                        fileStream.Read(Name, 0, 226);
                        string listname = "";

                        for (int j = 0; j < 226; j+=2)
                        {
                            int b = Name[j];
                            char a = (char)b;
                            listname += a;
                        }

                        listBox6.Items.Add(listname);
                        listBox7.Items.Add(Convert.ToString(i, 16));
                    }

                    if (Res == 3)
                    {
                        fileStream.Seek(i + 242, SeekOrigin.Begin);
                        byte[] Name = new byte[38];
                        string slistname = "";
                        fileStream.Read(Name, 0, 38);
                        for (int ji = 0; ji < 38; ji += 2)
                        {
                            int b = Name[ji];
                            char nb = (char)b;
                            slistname += nb;
                        } // перегон имени в стрингу
                            listBox9.Items.Add(slistname);
                            listBox8.Items.Add(slistname);
                        listBox9.Items.Add("=============BEGIN OF FOLDER===========");
                        listBox10.Items.Add(Convert.ToString(i, 16));
                        byte[] FlagOfDoll = new byte[1];
                        fileStream.Read(FlagOfDoll, 0, 1);
                        if (FlagOfDoll[0] == 36)
                        {
                            fileStream.Seek(i + 242 + 38 + 122, SeekOrigin.Begin);
                            for (int j = 0; j < 5; j++)
                            {
                                byte[] lengthofstruct = new byte[1];
                                fileStream.Seek(-74, SeekOrigin.Current);
                                fileStream.Read(lengthofstruct, 0, 1);
                                int structlength = lengthofstruct[0];

                                byte[] lengthofname = new byte[1];
                                fileStream.Seek(71, SeekOrigin.Current);
                                fileStream.Read(lengthofname, 0, 1);
                                int namelengthreal = lengthofname[0];
                                byte[] NewName = new byte[namelengthreal * 2];
                                fileStream.Read(NewName, 0, namelengthreal * 2);
                                string listname = "";
                                for (int ji = 1; ji < namelengthreal * 2; ji += 2)
                                {
                                    int b = NewName[ji];
                                    char nb = (char)b;
                                    listname += nb;
                                }
                                listBox9.Items.Add(listname);
                                fileStream.Seek(structlength - namelengthreal * 2 + 1, SeekOrigin.Current);
                            }
                        }
                        else
                        {
                            fileStream.Seek(i + 242 + 78 + 122, SeekOrigin.Begin);
                            for (int j = 0; j < 5; j++)
                            {
                                byte[] lengthofstruct = new byte[1];
                                fileStream.Seek(-74, SeekOrigin.Current);
                                fileStream.Read(lengthofstruct, 0, 1);
                                int structlength = lengthofstruct[0];

                                byte[] lengthofname = new byte[1];
                                fileStream.Seek(71, SeekOrigin.Current);
                                fileStream.Read(lengthofname, 0, 1);
                                int namelengthreal = lengthofname[0];
                                byte[] NewName = new byte[namelengthreal * 2];
                                fileStream.Read(NewName, 0, namelengthreal * 2);
                                string listname = "";
                                for (int ji = 1; ji < namelengthreal * 2; ji += 2)
                                {
                                    int b = NewName[ji];
                                    char nb = (char)b;
                                    listname += nb;
                                }
                                listBox9.Items.Add(listname);
                                fileStream.Seek(structlength - namelengthreal * 2 + 1, SeekOrigin.Current);
                            }
                        }
                        listBox9.Items.Add("=============END OF FOLDER=============");
                    }

                }
                //===================================================================================================================
                fileStream.Close();
            }
        }

        private void listBox6_SelectedIndexChanged(object sender, EventArgs e)
        {
            string adr = listBox7.Items[this.listBox6.SelectedIndex].ToString();
            int adress = int.Parse(adr, NumberStyles.AllowHexSpecifier); // получили адрес файла
            textBox2.Text = adr;
            string filename = textBox1.Text;
            FileStream fileStream = new FileStream(filename, FileMode.Open);
            StandardInformation SI;
            FileInformation FI;

            byte[] a = new byte[8];
            fileStream.Seek(adress + 80, SeekOrigin.Begin);
            ulong re = 0;

            fileStream.Read(a, 0, 8);
            for (int i = 0; i < 8; i++)
            {
                ulong eee = (ulong)(a[i] * System.Math.Pow(256, i));
                re += eee;
            }
            SI.FileCreationTime = re;
            re = 0;

            fileStream.Seek(adress + 88, SeekOrigin.Begin);
            fileStream.Read(a, 0, 8);
            for (int i = 0; i < 8; i++)
            {
                ulong eee = (ulong)(a[i] * System.Math.Pow(256, i));
                re += eee;
            }
            SI.FileAlterationTime = re;
            re = 0;

            fileStream.Seek(adress + 96, SeekOrigin.Begin);
            fileStream.Read(a, 0, 8);
            for (int i = 0; i < 8; i++)
            {
                ulong eee = (ulong)(a[i] * System.Math.Pow(256, i));
                re += eee;
            }
            SI.MFTChangeTime = re;
            re = 0;
            fileStream.Seek(adress + 104, SeekOrigin.Begin);
            fileStream.Read(a, 0, 8);
            for (int i = 0; i < 8; i++)
            {
                ulong eee = (ulong)(a[i] * System.Math.Pow(256, i));
                re += eee;
            }
            SI.FileReadTime = re;
            // SI заполнено

            fileStream.Seek(adress + 184, SeekOrigin.Begin);
            re = 0;

            fileStream.Read(a, 0, 8);
            for (int i = 0; i < 8; i++)
            {
                ulong eee = (ulong)(a[i] * System.Math.Pow(256, i));
                re += eee;
            }
            FI.FileCreationTime = re;
            re = 0;

            fileStream.Seek(adress + 192, SeekOrigin.Begin);
            fileStream.Read(a, 0, 8);
            for (int i = 0; i < 8; i++)
            {
                ulong eee = (ulong)(a[i] * System.Math.Pow(256, i));
                re += eee;
            }
            FI.FileModificationTime = re;
            re = 0;

            fileStream.Seek(adress + 200, SeekOrigin.Begin);
            fileStream.Read(a, 0, 8);
            for (int i = 0; i < 8; i++)
            {
                ulong eee = (ulong)(a[i] * System.Math.Pow(256, i));
                re += eee;
            }
            FI.FileAccessTime= re;
            re = 0;
            fileStream.Seek(adress + 208, SeekOrigin.Begin);
            fileStream.Read(a, 0, 8);
            for (int i = 0; i < 8; i++)
            {
                ulong eee = (ulong)(a[i] * System.Math.Pow(256, i));
                re += eee;
            }
            FI.MFTModificationTime = re;
            // FI заполнено
            double DataTime = 0;
            long DataTimeInt = 0;
            string printDate;
            listBox12.Items.Clear();
            listBox12.Items.Add(Convert.ToString((long)(SI.FileCreationTime), 16));

            DataTime = ((double)SI.FileCreationTime) / System.Math.Pow(10, 7);
            DataTimeInt = (long)DataTime;
            DataTimeInt = DataTimeInt - 11644473600;
            System.DateTime dateTime1 = new System.DateTime(1970, 1, 1, 0, 0, 0, 0);
            dateTime1 = dateTime1.AddSeconds(DataTimeInt);
            printDate = dateTime1.ToShortDateString() + " " + dateTime1.ToShortTimeString() + ':'+dateTime1.Second.ToString();
            listBox12.Items.Add(printDate);

            listBox12.Items.Add(Convert.ToString((long)(SI.FileAlterationTime), 16));

            DataTime = ((double)SI.FileAlterationTime) / System.Math.Pow(10, 7);
            DataTimeInt = (long)DataTime;
            DataTimeInt = DataTimeInt - 11644473600;
            System.DateTime dateTime2 = new System.DateTime(1970, 1, 1, 0, 0, 0, 0);
            dateTime2 = dateTime2.AddSeconds(DataTimeInt);
            printDate = dateTime2.ToShortDateString() + " " + dateTime2.ToShortTimeString() + ':' + dateTime1.Second.ToString();
            listBox12.Items.Add(printDate);

            listBox12.Items.Add(Convert.ToString((long)(SI.MFTChangeTime), 16));

            DataTime = ((double)SI.MFTChangeTime) / System.Math.Pow(10, 7);
            DataTimeInt = (long)DataTime;
            DataTimeInt = DataTimeInt - 11644473600;
            System.DateTime dateTime3 = new System.DateTime(1970, 1, 1, 0, 0, 0, 0);
            dateTime3 = dateTime3.AddSeconds(DataTimeInt);
            printDate = dateTime3.ToShortDateString() + " " + dateTime3.ToShortTimeString() + ':' + dateTime1.Second.ToString();
            listBox12.Items.Add(printDate);

            listBox12.Items.Add(Convert.ToString((long)(SI.FileReadTime), 16));

            DataTime = ((double)SI.FileReadTime) / System.Math.Pow(10, 7);
            DataTimeInt = (long)DataTime;
            DataTimeInt = DataTimeInt - 11644473600;
            System.DateTime dateTime4 = new System.DateTime(1970, 1, 1, 0, 0, 0, 0);
            dateTime4 = dateTime4.AddSeconds(DataTimeInt);
            printDate = dateTime4.ToShortDateString() + " " + dateTime4.ToShortTimeString() + ':' + dateTime1.Second.ToString();
            listBox12.Items.Add(printDate);

            listBox12.Items.Add(Convert.ToString((long)(FI.FileCreationTime), 16));

            DataTime = ((double)FI.FileCreationTime) / System.Math.Pow(10, 7);
            DataTimeInt = (long)DataTime;
            DataTimeInt = DataTimeInt - 11644473600;
            System.DateTime dateTime5 = new System.DateTime(1970, 1, 1, 0, 0, 0, 0);
            dateTime5 = dateTime5.AddSeconds(DataTimeInt);
            printDate = dateTime5.ToShortDateString() + " " + dateTime5.ToShortTimeString() + ':' + dateTime1.Second.ToString();
            listBox12.Items.Add(printDate);


            listBox12.Items.Add(Convert.ToString((long)(FI.FileModificationTime), 16));

            DataTime = ((double)FI.FileModificationTime) / System.Math.Pow(10, 7);
            DataTimeInt = (long)DataTime;
            DataTimeInt = DataTimeInt - 11644473600;
            System.DateTime dateTime6 = new System.DateTime(1970, 1, 1, 0, 0, 0, 0);
            dateTime6 = dateTime6.AddSeconds(DataTimeInt);
            printDate = dateTime6.ToShortDateString() + " " + dateTime6.ToShortTimeString() + ':' + dateTime1.Second.ToString();
            listBox12.Items.Add(printDate);

            listBox12.Items.Add(Convert.ToString((long)(FI.FileAccessTime), 16));

            DataTime = ((double)FI.FileAccessTime) / System.Math.Pow(10, 7);
            DataTimeInt = (long)DataTime;
            DataTimeInt = DataTimeInt - 11644473600;
            System.DateTime dateTime7 = new System.DateTime(1970, 1, 1, 0, 0, 0, 0);
            dateTime7 = dateTime7.AddSeconds(DataTimeInt);
            printDate = dateTime7.ToShortDateString() + " " + dateTime7.ToShortTimeString() + ':' + dateTime1.Second.ToString();
            listBox12.Items.Add(printDate);

            listBox12.Items.Add(Convert.ToString((long)(FI.MFTModificationTime), 16));

            DataTime = ((double)FI.MFTModificationTime) / System.Math.Pow(10, 7);
            DataTimeInt = (long)DataTime;
            DataTimeInt = DataTimeInt - 11644473600;
            System.DateTime dateTime8 = new System.DateTime(1970, 1, 1, 0, 0, 0, 0);
            dateTime8 = dateTime8.AddSeconds(DataTimeInt);
            printDate = dateTime8.ToShortDateString() + " " + dateTime8.ToShortTimeString() + ':' + dateTime1.Second.ToString();
            listBox12.Items.Add(printDate);

            fileStream.Close();


        }

        public static string ByteArrayToStringChar(byte[] ba)
        {
            string hex="";
            for (int i = 0; i < ba.Length; i++)
            {
                char ab = (char)ba[i];
                hex += ab;
            }
            return hex;
        }

        public static string ByteArrayToStringHex(byte[] ba)
        {
            string hex = "";
            string str11 = "";
            for (int i = 0; i < ba.Length; i++)
            {
                string hexstring = ba[i].ToString("X");
                if (hexstring.Length == 1)
                {
                    str11 += "0";
                    str11 += hexstring;
                    hexstring = str11;
                }
                hex += hexstring;
                hex += " ";
                str11 = "";
            }
            return hex;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            listBox13.Items.Clear();
            listBox13.Items.Add("================================================================");
            listBox14.Items.Clear();
            listBox14.Items.Add("================================================================");
            string filename = textBox1.Text;
            FileStream fileStream = new FileStream(filename, FileMode.Open);
            fileStream.Seek(int.Parse(textBox2.Text, NumberStyles.AllowHexSpecifier), SeekOrigin.Begin);
            byte[] a = new byte[64];

            for (int i = int.Parse(textBox2.Text, NumberStyles.AllowHexSpecifier); i < int.Parse(textBox2.Text, NumberStyles.AllowHexSpecifier) + 1024; i += 64)
            {
                fileStream.Read(a, 0, 64);
                string hexlinechar = ByteArrayToStringChar(a);
                string hexlinehex = ByteArrayToStringHex(a);

                listBox13.Items.Add(hexlinechar);
                listBox14.Items.Add(hexlinehex);
            }
            listBox13.Items.Add("================================================================");
            listBox14.Items.Add("================================================================");
            fileStream.Close();
        }


    }
}
