﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace greeeed
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void GetFileName()
        {
            string filename="";
            OpenFileDialog openFileDialog1 = new OpenFileDialog() { Filter = "Файлы образа|*.001" };
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
                filename = openFileDialog1.FileName;
            textBox1.Text = filename;
        } // получили имя файла

        private void button1_Click(object sender, EventArgs e)
        {
            GetFileName();
            button2.Enabled = true;
        }// записали имя файла

        private void button2_Click(object sender, EventArgs e)
        {
            listBox1.Visible = true;
            listBox1.Items.Clear();
            //================= дальше хуже
            string filename = textBox1.Text;
            FileStream fileStream = new FileStream(filename, FileMode.Open);
            /* открытие не проверяем, ибо файл открыт насильно и явно */


            for (int i = 450; i < 500; i += 16)
            {
                fileStream.Seek(i, SeekOrigin.Begin); // ищем точку монтирования
                byte[] NTFSmark = new byte[1];
                fileStream.Read(NTFSmark, 0, 1);

                if (NTFSmark[0] == 7)
                {
                    listBox1.Items.Add("Partition"); // проверяем NTFS и записываем его наличие
                }
            } // записали все boot-сектора
            fileStream.Close();
        }

        private int GetAdress(byte[] s5)
        {
            string s11 = new string('0', 2);
            string s12 = new string('0', 2);
            string s13 = new string('0', 2);
            string s14 = new string('0', 2);
            s11 = s5[0].ToString();
            s12 = s5[1].ToString();
            s13 = s5[2].ToString();
            s14 = s5[3].ToString();

            int a1, a2, a3, a4;
            a1 = int.Parse(s11);
            a2 = int.Parse(s12) * 256;
            a3 = int.Parse(s13) * 256 * 256;
            a4 = int.Parse(s14) * 256 * 256 * 256;
            int a = (a1 + a2 + a3 + a4) * 512;
            return a;
        }

        private int GetMFT(byte[] s6)
        {
            int a = s6[0] + s6[1] * 256 + s6[2] * 256 * 256 + s6[3] * 256 * 256 * 256;
            // да, явно не до конца, но для флешки на 256 метров самое ок
            return a;
        }
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox2.Items.Clear();
            listBox3.Items.Clear();
            int marker = this.listBox1.SelectedIndex;
            if (marker == -1)
                marker = -454;
            int boot = 454 + marker * 16;
            if (boot > 0)
            {
                splitContainer1.Visible = true;
                byte[] adress = new byte[4];
                byte[] size = new byte[4];
                byte[] MFT = new byte[8];
                string filename = textBox1.Text;
                FileStream fileStream = new FileStream(filename, FileMode.Open);
                fileStream.Seek(boot, SeekOrigin.Begin);
                fileStream.Read(adress, 0, 4);
                fileStream.Seek(boot + 4, SeekOrigin.Begin);
                fileStream.Read(size, 0, 4);
                int realadress = GetAdress(adress);
                int partsize = GetAdress(size);
                listBox2.Items.Add("File System:");
                listBox3.Items.Add("NTFS");
                listBox2.Items.Add("Partition size:");
                listBox3.Items.Add(partsize);
                listBox2.Items.Add("Boot Sector[dec]:");
                listBox3.Items.Add(realadress);
                string hexrealadress = Convert.ToString(realadress, 16);
                listBox2.Items.Add("Boot Sector[hex]:");
                listBox3.Items.Add(hexrealadress);
                int adr = realadress + 48; // вышли на 1 кусок MFT
                fileStream.Seek(adr, SeekOrigin.Begin);
                fileStream.Read(MFT, 0, 8);
                int mftaddr = GetMFT(MFT);
                listBox2.Items.Add("MFT[dec]:");
                listBox3.Items.Add(mftaddr);
                string hexmftaddr = Convert.ToString(mftaddr, 16);
                listBox2.Items.Add("MFT[hex]:");
                listBox3.Items.Add(hexmftaddr);
                int sum = mftaddr + realadress;
                string hexsum = Convert.ToString(sum, 16);
                listBox2.Items.Add("Real MFT[hex]:");
                listBox3.Items.Add(hexsum);
                fileStream.Close();
            }
        } //общая схема для любого раздела


    }
}
